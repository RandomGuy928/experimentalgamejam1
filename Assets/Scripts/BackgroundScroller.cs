﻿using UnityEngine;
using System.Collections;
using System;

public class BackgroundScroller : MonoBehaviour {

	public float scrollRate;

	float offset = 0;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.FindGameObjectWithTag("Manager");
		go.GetComponent<Stopwatch>().OnChange += FlipBackground;
	}
	
	// Update is called once per frame
	void Update () {
		offset += scrollRate*Time.deltaTime;
		renderer.material.SetTextureOffset ("_MainTex",new Vector2(offset, 0));
	}

	void FlipBackground(object sender, EventArgs e)
	{
		scrollRate *= -1;
	}
}
