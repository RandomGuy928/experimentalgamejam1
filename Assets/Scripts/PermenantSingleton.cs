﻿using UnityEngine;
using System.Collections;

public class PermenantSingleton : MonoBehaviour {

	public bool enforceSingle = true;

	// Use this for initialization
	void Start () {
		if(enforceSingle)
		{
			GameObject[] gos = GameObject.FindGameObjectsWithTag (gameObject.tag);
			foreach(GameObject go in gos)
			{
				if(!go.Equals (gameObject))
				{
					Destroy (gameObject);
					return;
				}
			}
		}

		DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
