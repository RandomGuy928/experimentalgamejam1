﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

	public AudioClip[] songs;
	public int currentSongIndex;
	public bool playing;

	AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
		playing = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(playing && !audioSource.isPlaying)
		{
			PlayRandomSong();
		}
	}

	void PlayRandomSong()
	{
		int rand = Random.Range (0, songs.Length);
		if(rand == currentSongIndex)
		{
			PlayRandomSong ();
			return;
		}
		currentSongIndex = rand;
		audioSource.clip = songs[rand];
		audioSource.Play ();
	}

	public void Stop()
	{
		playing = false;
		audioSource.Stop ();
	}

	public void Play()
	{
		playing = true;
		PlayRandomSong();
	}
}
