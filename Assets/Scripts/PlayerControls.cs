﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerControls : MonoBehaviour {

	//Sound event triggers
	public event EventHandler OnHit;
	public event EventHandler OnPunch;
	public event EventHandler OnJump;
	//public event EventHandler OnDie;

    // Global Variables
	public enum PlayerEnum {Player1, Player2};
	public PlayerEnum player;
	public Texture2D healthPip;

	public bool faceRightAtStart;

    public float jumpForce;
    public float groundedWalkForce;
	public float flyingWalkForce;
	public float spikeForce;
	public float maxWalkSpeed;
	public float hitForce;

	public float jumpHoldDuration;
	public float jumpHoldForce;

	public float overspeedDeceleration;

	public float hitTime;
	public float punchTime;
	public float cooldownTime;

	public int health;

	float gravityScale;
	float jumpHoldTimer;
	float hitTimer;
	float punchTimer;
	float cooldownTimer;
	bool jumping;
	bool hit;
	bool punching;
	bool cooldown;

	Vector3 spawnPos;

	PlayerControls footstoolTarget;

	Collider2D fist;

	public int midairJumps;
	int currentMidairJumps;

	public float platformDetectionRange;
	public LayerMask platformDetection;

    KeyController keyController;
    public KeyCode keyJump,
                   keyLeft,
                   keyRight,
                   keyPunch;

	public GameObject jumpSprite,
				      leftSprite,
	                  rightSprite,
	                  punchSprite;

	public bool grounded;

	List<GameObject> platformChecks = new List<GameObject>();

	PlayerAnimator animator;

	float xScale;

    // Use this for initialization
    void Start (){
		spawnPos = transform.position;
		xScale = transform.localScale.x;
		animator = GetComponent<PlayerAnimator>();
        keyController = GameObject.Find("Manager").GetComponent<KeyController>();

		if(faceRightAtStart)
		{
			Vector3 scale = transform.localScale;
			scale.x *= -1;
			transform.localScale = scale;
		}

		gravityScale = rigidbody2D.gravityScale;

		for(int i = 0; i < transform.childCount; i++)
		{
			GameObject go = transform.GetChild (i).gameObject;
			if(go.tag == "PlatformCheck")
			{
				platformChecks.Add (go);
			}
			if(go.tag == "Fist")
			{
				fist = go.GetComponent<Collider2D>();
			}
		}

		UpdateKeySprites();
    }

	//Physics update
	void FixedUpdate()
	{
		Vector2 vel = rigidbody2D.velocity;
		if(grounded || jumping)
		{
			rigidbody2D.gravityScale = 0;
		}
		else
		{
			rigidbody2D.gravityScale = gravityScale;
		}
		if(hit && grounded)
		{
			vel.x = Mathf.Max (Mathf.Abs (vel.x) - overspeedDeceleration*Time.deltaTime*5, 0) * Mathf.Sign (vel.x);
		}
		else if(Mathf.Abs (vel.x) > maxWalkSpeed)
		{
			vel.x = Mathf.Max (Mathf.Abs (vel.x) - overspeedDeceleration*Time.deltaTime, maxWalkSpeed) * Mathf.Sign (vel.x);
		}
		rigidbody2D.velocity = vel;
	}

    // Update is called once per frame
    void Update () {
		ProcessHitRecovery();
		ProcessPunchRecovery();
		DetectFloor();
		ProcessInput();

    }

	public void UpdateKeySprites()
	{
		leftSprite.GetComponent<SpriteRenderer>().sprite = keyController.GetSprite(keyLeft);
		rightSprite.GetComponent<SpriteRenderer>().sprite = keyController.GetSprite(keyRight);
		jumpSprite.GetComponent<SpriteRenderer>().sprite = keyController.GetSprite(keyJump);
		punchSprite.GetComponent<SpriteRenderer>().sprite = keyController.GetSprite(keyPunch);
	}

	public void TakeDamage()
	{
		health--;
	}

	public void Respawn()
	{
		TakeDamage();
		if(health <= 0)
		{
			return;
		}
		rigidbody2D.velocity = Vector2.zero;
		transform.position = spawnPos;
	}

	private void ProcessHitRecovery()
	{
		if(hit)
		{
			hitTimer += Time.deltaTime;
			if(hitTimer > hitTime)
			{
				hit = false;
			}
		}
	}

	private void ProcessPunchRecovery()
	{
		if(punching)
		{
			punchTimer += Time.deltaTime;
			if(punchTimer > punchTime)
			{
				punching = false;
				fist.enabled = false;
				cooldown = true;
				cooldownTimer = 0f;
				if(!grounded)
				{
					animator.Midair();
				}
			}
		}

		if(cooldown)
		{
			cooldownTimer += Time.deltaTime;
			if(cooldownTimer > cooldownTime)
			{
				cooldown = false;
			}
		}
	}

	private void DetectFloor()
	{
		grounded = false;
		footstoolTarget = null;
		foreach(GameObject check in platformChecks)
		{
			Vector3 newPos = check.transform.position;
			newPos.y -= platformDetectionRange;
			Debug.DrawLine (check.transform.position, newPos);
			RaycastHit2D hit = Physics2D.Raycast(check.transform.position, -Vector2.up, platformDetectionRange, platformDetection);
			if(hit.collider != null)
			{
				grounded = true;
				if(hit.collider.gameObject.tag == "Player")
				{
					footstoolTarget = hit.collider.gameObject.GetComponent<PlayerControls>();
				}
				else
				{
					currentMidairJumps = midairJumps;
				}
			}
		}
	}

	public void Footstool()
	{
		rigidbody2D.AddForce (new Vector2(0, -spikeForce));
		Hit (0, true);
	}

	private void ProcessInput()
	{
		if(!punching && !hit && !cooldown && Input.GetKeyDown (keyPunch))
		{
			Punch ();
		}

		if (!punching && !hit && (grounded || currentMidairJumps > 0) && Input.GetKeyDown(keyJump))
		{
			Vector2 vel = rigidbody2D.velocity;
			if(!grounded)
			{
				currentMidairJumps--;
				if(Input.GetKey (keyLeft))
				{
					vel.x = -maxWalkSpeed;
				}
				if(Input.GetKey (keyRight))
				{
					vel.x = maxWalkSpeed;
				}
			}
			animator.Jump ();
			vel.y = 0;
			rigidbody2D.velocity = vel;
			rigidbody2D.AddForce(new Vector2(0, jumpForce));
			jumping = true;
			jumpHoldTimer = 0f;
			if(footstoolTarget != null)
			{
				footstoolTarget.Footstool();
			}
			if(OnJump != null)
			{
				OnJump(gameObject, new EventArgs());
			}
		}

		if(Input.GetKeyUp (keyJump))
		{
			jumping = false;
		}

		if(jumping && Input.GetKey (keyJump))
		{
			jumpHoldTimer += Time.deltaTime;
			if(jumpHoldTimer >= jumpHoldDuration)
			{
				jumping = false;
			}
			else
			{
				rigidbody2D.AddForce (new Vector2(0, jumpHoldForce *  Time.deltaTime));
			}
		}
		
		if (!punching && !hit && Input.GetKey(keyLeft))
		{
			FaceLeft();
			if(grounded && !jumping)
			{
				animator.Walk ();
			}
			if(rigidbody2D.velocity.x > -maxWalkSpeed)
			{
				rigidbody2D.AddForce(new Vector2(-GetWalkForce () * Time.deltaTime, 0));
			}
		}
		
		if (!punching && !hit && Input.GetKey(keyRight))
		{
			FaceRight ();
			if(grounded && !jumping)
			{
				animator.Walk ();
			}
			if(rigidbody2D.velocity.x < maxWalkSpeed)
			{
				rigidbody2D.AddForce(new Vector2(GetWalkForce () * Time.deltaTime, 0));
			}
		}

		if(grounded && punching && !hit)
		{
			Vector2 vel = rigidbody2D.velocity;
			vel.x = 0;
			rigidbody2D.velocity = vel;
		}
		
		if(grounded && !jumping && !punching && !hit && !Input.GetKey (keyRight) && !Input.GetKey (keyLeft))
		{
			Vector2 vel = rigidbody2D.velocity;
			vel.x = 0;
			rigidbody2D.velocity = vel;
			animator.Stand();
		}
	}

	public void Punch()
	{
		punching = true;
		punchTimer = 0f;
		fist.enabled = true;
		if(grounded)
		{
			animator.Punch();
		}
		else
		{
			animator.PunchAir ();
		}
		if(OnPunch != null)
		{
			OnPunch(gameObject, new EventArgs());
		}
	}

	public void Hit(float direction, bool footstool = false)
	{
		TakeDamage();
		if(!footstool)
		{
			Vector2 vel = rigidbody2D.velocity;
			vel.x = 0;
			rigidbody2D.velocity = vel;
			rigidbody2D.AddForce (new Vector2(Mathf.Sign (direction) * hitForce, 0));
		}
		if(grounded)
		{
			animator.Hit();
		}
		else
		{
			animator.HitAir ();
		}
		hit = true;
		hitTimer = 0f;

		if(direction == 0)
		{
			direction = 1;
		}
		if(OnHit != null)
		{
			OnHit(gameObject, new EventArgs());
		}
	}

	public float GetWalkForce()
	{
		if(grounded)
		{
			return groundedWalkForce;
		}
		return flyingWalkForce;
	}

	private void FaceRight()
	{
		Vector3 scale = transform.localScale;
		scale.x = -xScale;
		transform.localScale = scale;
	}

	private void FaceLeft()
	{
		Vector3 scale = transform.localScale;
		scale.x = xScale;
		transform.localScale = scale;
	}

    public void ReturnKeys() {

		jumping = false;
        keyController.AddKey(keyLeft);
        keyController.AddKey(keyRight);
        keyController.AddKey(keyJump);
        keyController.AddKey(keyPunch);

    }

    public void GetKeys() {

        keyLeft  = keyController.GetKey();
        keyRight = keyController.GetKey();
        keyJump  = keyController.GetKey();
        keyPunch = keyController.GetKey();

		UpdateKeySprites();
    }

	public void OnGUI()
	{
		float y = Screen.height * 0.885f;
		float height = Screen.height * 0.035f;
		float startPos;
		float increment;
		if(player == PlayerEnum.Player1)
		{
			startPos = Screen.width * 0.175f;
			increment = Screen.width * 0.001f + height;
		}
		else
		{
			startPos = Screen.width * 0.825f;
			increment = -(Screen.width * 0.001f + height);
		}

		GUI.skin.label.stretchHeight = true;
		GUI.skin.label.stretchWidth = true;
		for(int i = 0; i < health; i++)
		{
			GUI.Label (new Rect(startPos + i*increment, y, height, height), healthPip);
		}
	}
}
