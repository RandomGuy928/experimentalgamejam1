﻿using UnityEngine;
using System.Collections;
using System;

public class FlowController : MonoBehaviour {

	public event EventHandler OnStart;
	public event EventHandler OnWin;

	public enum GameState {Title, Play, Win};
	public GameState state;

	public float time;

	float timer;

	PlayerControls[] players;
	bool ended;

	// Use this for initialization
	void Start () {
		OnLevelWasLoaded();
	}
	
	// Update is called once per frame
	void Update () {
		Screen.showCursor = false;

		timer += Time.deltaTime;

		if(state == GameState.Play && players!=null && players.Length > 0 && !ended)
		{
			foreach(PlayerControls player in players)
			{
				if(player.health <= 0)
				{
					VictoryScreen(player.player);
				}
			}
		}

		if(timer < time)
		{
			return;
		}

		if(state == GameState.Title && Input.anyKeyDown)
		{
			if(OnStart != null)
			{
				OnStart(gameObject, new EventArgs());
			}
			Application.LoadLevel("master");
		}
		if(state == GameState.Win && Input.anyKeyDown)
		{
			Application.LoadLevel ("title");
		}
	}

	void OnLevelWasLoaded () 
	{
		timer = 0f;
		ended = false;
		GameObject[] gos = GameObject.FindGameObjectsWithTag("Player");
		players = new PlayerControls[gos.Length];
		for(int i = 0; i < gos.Length; i++)
		{
			state = GameState.Play;
			players[i] = gos[i].GetComponent<PlayerControls>();
		}

	}

	void VictoryScreen(PlayerControls.PlayerEnum player)
	{
		ended = true;
		state = GameState.Win;
		if(OnWin != null)
		{
			OnWin(gameObject, new EventArgs());
		}
		if(player == PlayerControls.PlayerEnum.Player1)
		{
			Application.LoadLevel ("player2win");
		}
		else
		{
			Application.LoadLevel("player1win");
		}
	}
}
