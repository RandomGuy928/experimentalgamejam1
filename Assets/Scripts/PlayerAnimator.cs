﻿using UnityEngine;
using System.Collections;

public class PlayerAnimator : MonoBehaviour {

	public string playerAnimationPrefix;

	public Animator animator;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Midair()
	{
		animator.Play(playerAnimationPrefix + "Midair");
	}

	public void Jump()
	{
		animator.Play (playerAnimationPrefix + "Jump");
	}

	public void Walk()
	{
		animator.Play (playerAnimationPrefix + "Walk");
	}

	public void Stand()
	{
		animator.Play (playerAnimationPrefix + "Stand");
	}

	public void Hit()
	{
		animator.Play(playerAnimationPrefix + "Hit");
	}
	
	public void HitAir()
	{
		animator.Play(playerAnimationPrefix + "HitAir");
	}
	
	public void Punch()
	{
		animator.Play(playerAnimationPrefix + "Punch");
	}
	
	public void PunchAir()
	{
		animator.Play(playerAnimationPrefix + "PunchAir");
	}
}
